class Scale
  include ActiveModel::Model
  attr_accessor :name

  class << self
    def all
      JSON.parse(scales_file).keys.sort
    end

    private

      def scales_file
        File.read(scales_file_path)
      end

      def scales_file_path
        Rails.root.join('node_modules', 'tonal-dictionary', 'data', 'scales.json')
      end
  end
end
