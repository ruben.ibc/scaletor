class Chord
  include ActiveModel::Model
  attr_accessor :name

  class << self
    def all
      JSON.parse(chords_file).keys
    end

    private

      def chords_file
        File.read(chords_file_path)
      end

      def chords_file_path
        Rails.root.join('node_modules', 'tonal-dictionary', 'data', 'chords.json')
      end
  end
end
