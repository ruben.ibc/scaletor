class Note
  include ActiveModel::Model
  attr_accessor :name

  def self.all
    %w[C  C#
       Db D D#
       Eb E
       F  F#
       Gb G G#
       Ab A A#
       Bb B
    ]
  end
end
