import { Controller } from 'stimulus'
import { Note, Interval, Distance, Scale, Chord } from 'tonal'

export default class extends Controller {
  static targets = [ 'resultScale', 'resultChord', 'key', 'scale', 'chord' ]

  updateScale () {
    this.resultScaleTarget.innerHTML = Scale.notes(this.scaleName)
  }

  updateChord () {
    this.resultChordTarget.innerHTML = Chord.notes(this.chordName)
  }

  // helper functions to get scales, chords and keys

  get scaleName() {
    return `${this.key} ${this.scale}`
  }

  get chordName() {
    return `${this.key}${this.chord}`
  }

  get key() {
    return this.keyTarget.options[this.keyTarget.selectedIndex].value
  }

  get scale() {
    return this.scaleTarget.options[this.scaleTarget.selectedIndex].value
  }

  get chord() {
    return this.chordTarget.options[this.chordTarget.selectedIndex].value
  }
}
